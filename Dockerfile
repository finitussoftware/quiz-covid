FROM openjdk:jre-alpine
RUN mkdir /app
COPY build/libs/covid-0.0.1-SNAPSHOT.jar /app/covid.jar
WORKDIR /app
CMD java -jar covid.jar