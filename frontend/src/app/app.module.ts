/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LOCALE_ID, NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import es from '@angular/common/locales/es-MX';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule, NbTimepickerModule,
  NbToastrModule,
  NbWindowModule
} from '@nebular/theme';
import {GraphQLModule} from './graphql.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {CurrencyPipe, DatePipe, registerLocaleData} from '@angular/common';
import {AuthModule} from './auth/auth.module';
registerLocaleData(es);

@NgModule({
  declarations: [AppComponent],
  providers: [{ provide: LOCALE_ID, useValue: 'es-MX' },
    DatePipe, CurrencyPipe],
  imports: [
    NbToastrModule.forRoot(),
    BrowserModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    SweetAlert2Module.forRoot(),
    ThemeModule.forRoot(),
    NbTimepickerModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),

    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    AuthModule.forRoot(),
    ThemeModule.forRoot(),
    GraphQLModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
