import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import {ThemeModule} from '../@theme/theme.module';
import {NbAuthModule} from '@nebular/auth';
import {NbAlertModule} from '@nebular/theme';
import {NbInputModule} from '@nebular/theme';
import {FormsModule} from '@angular/forms';
import {NbCheckboxModule} from '@nebular/theme';
import {NbButtonModule} from '@nebular/theme';
import {NbIconModule} from '@nebular/theme';
import {ModuleWithProviders} from '@angular/core';
import {AuthGuard} from './auth-guard';
import {NbPasswordAuthStrategy} from '@nebular/auth';
import {NbAuthJWTToken} from '@nebular/auth';
import {HttpResponse} from '@angular/common/http';
import {NbPasswordAuthStrategyOptions} from '@nebular/auth';
import {getDeepFromObject} from '@nebular/auth';
import {NbRoleProvider} from '@nebular/security';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {NbAuthJWTInterceptor} from '@nebular/auth';
import {NbTokenStorage} from '@nebular/auth';
import {NbTokenLocalStorage} from '@nebular/auth';
import {NbSimpleRoleProvider} from '../@core/core.module';
import {NB_AUTH_TOKEN_INTERCEPTOR_FILTER} from '@nebular/auth';
import {HttpRequest} from '@angular/common/http';
import {NbAuthOAuth2JWTToken} from '@nebular/auth';
import {environment} from '../../environments/environment';

export function tokenInterceptorFilter(req: HttpRequest<any>) {
  return [`${environment.backendBaseUrl}/api/auth/refresh`, `${environment.backendBaseUrl}/api/auth/signin`].includes(req.url);
  // return ['/api/auth/signin', '/api/auth/signout'].includes(req.url);
  // return false;
}

export function getAuthJwtToken(module: string, res: HttpResponse<Object>, options: NbPasswordAuthStrategyOptions) {
  return getDeepFromObject(res.body, options.token.key);
}

@NgModule({
  declarations: [LoginComponent, LogoutComponent],
  imports: [
    ThemeModule,
    NbAuthModule,
    AuthRoutingModule,
    NbAlertModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbButtonModule,
    NbIconModule,
  ],
})
export class AuthModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return {
      ngModule: AuthModule,
      providers: [
        AuthGuard,

        ...NbAuthModule.forRoot({
          strategies: [
            NbPasswordAuthStrategy.setup({
              name: 'email',

              register: false,
              requestPass: false,
              resetPass: false,
              baseEndpoint: `${environment.backendBaseUrl}/api/auth`,
              login: {
                endpoint: '/signin',
              },
              logout: {
                endpoint: '/logout',
              },
              refreshToken: {
                endpoint: '/refresh',
              },
              token: {
                class: NbAuthJWTToken,
                key: 'data.key',
                getter: getAuthJwtToken,
              },
            }),
          ],
        }).providers,
        { provide: NbRoleProvider, useClass: NbSimpleRoleProvider},
        { provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true},
        { provide: NbTokenStorage, useClass: NbTokenLocalStorage },
        { provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER, useValue: tokenInterceptorFilter},
      ],
    };
  }
}
