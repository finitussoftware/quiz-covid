import {Component} from '@angular/core';
import {Inject} from '@angular/core';
import {ChangeDetectorRef} from '@angular/core';
import {ChangeDetectionStrategy} from '@angular/core';
import {getDeepFromObject, NbTokenService} from '@nebular/auth';
import {NbAuthResult} from '@nebular/auth';
import {NB_AUTH_OPTIONS} from '@nebular/auth';
import {NbAuthService} from '@nebular/auth';
import {NbAuthSocialLink} from '@nebular/auth';
import {Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import * as _ from "lodash";

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;
  socialLinks: NbAuthSocialLink[] = [];
  rememberMe = false;

  constructor(
    private tokenService: NbTokenService,
    private http: HttpClient,
    protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS) protected options = {},
              protected cd: ChangeDetectorRef,
              protected router: Router) {

    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.strategy = this.getConfigValue('forms.login.strategy');
    this.socialLinks = this.getConfigValue('forms.login.socialLinks');
    this.rememberMe = this.getConfigValue('forms.login.rememberMe');
  }

  login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;
      this.service.authenticate(this.strategy, this.user).subscribe((result: NbAuthResult) => {
        this.submitted = false;

        if (result.isSuccess()) {
          localStorage.setItem('currentUser',this.user.email)
          this.messages = result.getMessages();
        } else {
          this.errors = result.getErrors();
        }

        const redirect = result.getRedirect();
        if (redirect) {
          return this.router.navigate(['pages/'])
        }
        this.cd.detectChanges();
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
