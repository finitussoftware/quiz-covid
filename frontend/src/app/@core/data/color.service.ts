import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  public colors: any[][] = [[],[],[]];

  constructor() { }

  public pow(x: any,a: any){
    return Math.pow(2,x)*a
  }


  public fillColors(i: any, color: any, step: any, el: any ){
    let a = 255, b = 0, c = 0;
    if(step == 1)
      a = (i*this.pow(8-el,1))+127
    if(step == 2)
      b = ((i-this.pow(el,0.25))*(this.pow(9-el,1)))-1
    if(b<0) b+=1
    if(step == 3){
      a =  (255-(this.pow(9-el,1)*(i-this.pow(el,0.75))))-1
      b = 255
    }
    if(color == 0) return {red: a, green: b, blue: c}
    if(color == 1) return {red: c, green: a, blue: b}
    return {red: b, green: c, blue: a}
  }

  public defineColors(x: any, y: any){
    if(x == 0) return
    let a=0;
    while(x>this.pow(a,1)){
      a+=1
    }
    for(let i = 0; i<this.pow(a,1); i++){
      if(i<this.pow(a,0.25))
        this.colors[y][i] = this.fillColors(i,y, 1, a)
      if(i>=this.pow(a,0.25) && i <=this.pow(a,0.75))
        this.colors[y][i]= this.fillColors(i,y, 2, a)
      if(i>this.pow(a,0.75) && i<=this.pow(a,1))
        this.colors[y][i] = this.fillColors(i,y, 3,a)
    }
    return this.colors[y]
  }
}
