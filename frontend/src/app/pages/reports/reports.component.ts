import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'ngx-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {

  public employees: any[];
  private httpClient: HttpClient;

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  ngOnInit(): void {
    this.httpClient.get('/api/covid/answers').subscribe(employees => this.employees = <any[]>employees);
  }

  onChangeStatus(employee) {
    this.httpClient.post('api/covid/answers', {}, {
      params: {
        employeeId: employee.employee_number,
        status: employee.status,
      },
    }).subscribe(console.log);
  }
}
