import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {NbActionsModule, NbSelectModule} from "@nebular/theme";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ReportsComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    NgxDatatableModule,
    NbActionsModule,
    NbSelectModule,
    FormsModule
  ]
})
export class ReportsModule { }
