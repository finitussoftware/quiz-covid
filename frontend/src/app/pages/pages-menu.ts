import { NbMenuItem } from '@nebular/theme';
export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Reportes',
    link: '/pages/dashboard',
    data: {
      permission: 'VIEW_REPORTS',
    }
  },
  {
    title: 'Quiz',
    link: '/pages/quiz'
  },
  {
    title: 'Quiz Answers',
    link: '/pages/reports/employees',
    data: {
      permission: 'VIEW_ADMIN',
    }
  },
  {
    title: 'Administracion',
    data: {
      permission: 'VIEW_ADMIN',
    },
    children: [
      {
        title: 'Usuarios',
        icon: 'person-outline',
        link: '/pages/admin/users',
      },
      {
        title: 'Empleados',
        icon: 'person-outline',
        link: '/pages/admin/employees',
      },],},
];
