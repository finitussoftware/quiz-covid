import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsersComponent} from "./users/users.component";
import {EmployeesComponent} from "./employees/employees.component";
const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
  },
  {
    path: 'employees',
    component: EmployeesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
