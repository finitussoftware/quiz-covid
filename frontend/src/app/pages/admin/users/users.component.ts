import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {map} from 'rxjs/operators';
import * as _ from 'lodash';
import {Role, User} from '../../../../generated/graphql';
import {Query} from '../../../../generated/graphql';
import {NbToastrService} from "@nebular/theme";

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  public saveButton = true;
  public roles: Role[];
  public users: any;
  public userCopy: any;
  public selectedUser: any;
  public passwordValidation: any;

  constructor(private apollo: Apollo,
              private toastrService: NbToastrService,
  ) { }

  ngOnInit(): void {
    this.loadUsers();
    this.getRoles().subscribe(roles=>{this.roles = roles;})
  }

  private getUsers(): Observable<any> {
    return this.apollo.query<any>({
      query: gql`{
        users {
          id username
        }
      }`
    }).pipe(map(response => response.data.users));
  }

  public getRoles(): Observable<Role[]> {
    let query = gql`query {
      roles {
        id
        name
      }
    }`;
    return this.apollo.query<Query>({
      query: query,
    }).pipe(map(x => x.data.roles));
  }

  public getRol(id: number): Observable<number> {
    let query = gql` query ($id: Int) {
      rolById (id: $id)
    }`;

    return this.apollo.query<Query>({
      query: query,
      variables: {
        id: id
      },
    }).pipe(map(x => x.data.rolById));
  }

  public loadUsers() {
    this.getUsers().subscribe(users => {this.users = users;
      /*this.users.forEach(user =>{
        this.getRol(user.id).subscribe(rol => {user.rolId = rol;
        })
      })*/}
    );
  }

  onNewUser() {
    this.selectedUser = {};
    this.passwordValidation = null
  }

  public passEmpty(){
    this.saveButton = _.isEmpty(this.selectedUser.password) || _.isEmpty(this.passwordValidation)
  }

  public compare(){
    if(this.userCopy == null)
      return this.saveButton = (this.selectedUser.username == null || this.selectedUser.rolId == null || this.selectedUser.password == null || this.passwordValidation  == null)
    this.saveButton = _.isEqual(this.selectedUser.username, this.userCopy.username) && _.isEqual(this.selectedUser.rolId, this.userCopy.rolId)
  }


  onSaveUser() {
    if(this.users.find(x=>x.username.toUpperCase() == this.selectedUser.username.toUpperCase() && !this.selectedUser.id)){
      this.showToast('Ya hay un usuario con el nombre '+ this.selectedUser.username,'top-right', 'warning','Nombre duplicado');
      return
    }
    if(this.selectedUser.password == this.passwordValidation){
      this.saveUser()
        .subscribe(() => {
          this.showToast('Registro guardado con el nombre '+ this.selectedUser.username,'top-right', 'success', null);
          this.loadUsers();
          this.onCancelUser();
        });
    }
    else{this.showAlertToast('Las contraseñas no coinciden','top-right', 'warning');
    }
  }

  public onCancelUser() {
    this.selectedUser = null;
    this.passwordValidation = null
    this.userCopy = null
    this.saveButton = true
  }

  public saveUser(): Observable<any> {
    return this.apollo.mutate({
      mutation: gql`mutation ($user: UserInput) {
        saveUser(user: $user)
      }`,
      variables: {
        user: this.selectedUser
      }
    });
  }

  private deleteUser(userId: any) {
    return this.apollo.mutate<any>({
      mutation: gql`mutation ($id: Int) {
        deleteUser(id: $id)
      }`,
      variables: {id: userId}
    }).toPromise();
  }

  showToast(message, position, status, title) {
    this.toastrService.show(
      message || 'Success',
      title || `Registro guardado`,
      {position, status});
  }

  showAlertToast(message, position, status) {
    this.toastrService.show(
      message || 'warning',
      `Registro no guardado`,
      {position, status});
  }


  onSelectUser(user: User) {
    this.selectedUser = _.cloneDeep(user);
    this.userCopy = _.cloneDeep(user)
    this.passwordValidation = null;
    this.saveButton = true
  }

  onDeleteUser(user: any) {
    this.deleteUser(user.id).then(() => {this.showToast('Se ha eliminado el usuario con el nombre ' + user.username,'top-right', 'success','Registro eliminado');
    this.loadUsers();
    this.onCancelUser();})
  }
}
