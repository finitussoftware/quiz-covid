import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import {
  NbAutocompleteModule,
  NbCardModule,
  NbCheckboxModule, NbContextMenuModule, NbDatepickerModule,
  NbFormFieldModule, NbPopoverModule,
  NbRadioModule, NbTabsetModule,
  NbTimepickerModule, NbToggleModule,
  NbTooltipModule
} from '@nebular/theme';
import {NbListModule} from '@nebular/theme';
import {NbInputModule} from '@nebular/theme';
import {FormsModule} from '@angular/forms';
import {NbIconModule} from '@nebular/theme';
import {NbButtonModule} from '@nebular/theme';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import {NbSelectModule} from '@nebular/theme';
import {UsersComponent} from "./users/users.component";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {NbMomentDateModule} from "@nebular/moment";
import {EmployeesComponent} from "./employees/employees.component";

@NgModule({
  declarations: [
    EmployeesComponent,
    UsersComponent,],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NbCardModule,
    NbListModule,
    NbInputModule,
    FormsModule,
    NbIconModule,
    NbButtonModule,
    SweetAlert2Module,
    NbSelectModule,
    NbCheckboxModule,
    NbTimepickerModule,
    NbTooltipModule,
    NbRadioModule,
    NbFormFieldModule,
    NbAutocompleteModule,
    NbTabsetModule,
    NbPopoverModule,
    NbToggleModule,
    NgxDatatableModule,
    NbContextMenuModule,
    NbDatepickerModule,
    NbMomentDateModule,
  ],
})
export class AdminModule { }
