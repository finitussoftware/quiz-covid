import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {map} from 'rxjs/operators';
import * as _ from 'lodash';
import {NbPosition, NbToastrService} from '@nebular/theme';
import {Employee, Mutation, MutationSaveEmployeesArgs} from '../../../../generated/graphql';
import {ExportToCsv} from 'export-to-csv';

@Component({
  selector: 'ngx-users',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
})
export class EmployeesComponent implements OnInit {
  position: NbPosition = NbPosition.BOTTOM;
  public options = {
    filename: 'Reporte de errores',
    showLabels: true,
    useTextFile: false,
    useKeysAsHeaders: true};
  public csvExporter = new ExportToCsv(this.options);
  public toUploadEmployees: Employee[] = [];
  public file: any;
  public employee: Employee = {};
  public print: boolean = false;
  public usedIDs: any;
  public a: any;
  public errorReport: any;
  constructor(private apollo: Apollo,
              private toastrService: NbToastrService,
  ) { }

  ngOnInit(): void {
  }

  public onClickUpload() {
    this.file = document.getElementById('employees');
    this.usedIDs = [];
    if (this.file.files === undefined) return;
    if (this.file.files[0].name.split('.')[1] !== 'csv')
      return this.errorToast('El formato del archivo no es del tipo SCV', 'top-right', 'error');
    this.a = this.file.files[0];
    const reader = new FileReader();
    reader.readAsText(this.a);
    reader.onload = () => {
      const arr = reader.result.toString().split(/[\r\n]/);
      const rows = [];
      arr.forEach(x => {if (x.length > 0) rows.push(x.split(',')); });
      this.sendDinersData(rows);
    };
  }

  public toUpper(a: any) {
    return a ? a.toUpperCase() : null;
  }


  public sendDinersData(rows: any) {
    this.print = false;
    this.errorReport = [];
    let error = [];
    if (rows.length < 2) this.exportEmployees(new Array(5).fill(''), 'No hay datos de empleados');
    for (let i = 1; i < rows.length; i++) {
      error = [];
      if (!rows[i])
        return this.exportEmployees(rows[i], 'No hay datos de empleado');
      this.employee.employeeNumber = this.toUpper(rows[i][0]);
      this.employee.name = this.toUpper(rows[i][1]);
      this.employee.middleName = this.toUpper(rows[i][2]);
      this.employee.lastName = this.toUpper(rows[i][3]);
      this.employee.number = !_.isNaN(parseFloat(rows[i][4])) ? this.toUpper(rows[i][4]) : null;
      this.employee.area = this.toUpper(rows[i][5]);
      this.employee.supervisor = this.toUpper(rows[i][6]);
      this.employee.position = this.toUpper(rows[i][7]);
      this.employee.shift = this.toUpper(rows[i][8]);
      this.employee.gender = this.toUpper(rows[i][9]);
      if (this.usedIDs.find(x => x === rows[i][0]))
        error.push('Esta ID ya ha sido utilizado para otro registro en el documento');
      this.usedIDs.push(rows[i][0]);
      if (!this.employee.employeeNumber)
        error.push('El campo numero de empleado es obligatorio');
      if (!this.employee.name)
        error.push('El campo nombre es obligatorio');
      let errors = '';

      if (error.length === 0) {
        this.exportEmployees(rows[i], 'OK');
        this.toUploadEmployees.push(_.cloneDeep(this.employee));
      } else {
        this.print = true;
        error.forEach(x => errors += x + ', ');
        this.exportEmployees(rows[i], errors.substring(0, errors.length - 2));
      }

      if (this.print === true) {
        const index = this.errorReport.lastIndexOf(x => x.Error !== 'No hay datos de empleado');
        if (rows.length > 2 && index !== -1)
          this.errorReport.splice(index);
        this.csvExporter.generateCsv(this.errorReport);
      }
      this.errorReport = [];
      this.employee = {};
    }

    if (!_.isEmpty(this.toUploadEmployees)) {
      this.saveEmployees().subscribe(res => {
        if (res)
          this.showToast('Se han registrado ' + this.toUploadEmployees.length + ' nuevo(s) empleado(s).', 'top-right', 'success');
        this.toUploadEmployees = [];
      });
    }
  }

  showToast(message, position, status) {
    this.toastrService.show(
      message || 'Success',
      `Registro guardado`,
      {position, status});
  }

  public saveEmployees(): Observable<boolean> {
    return this.apollo.mutate<Mutation, MutationSaveEmployeesArgs>({
      mutation: gql`mutation ($employees: [EmployeeInput]) {
        saveEmployees(employees: $employees)
      }`,
      variables: {
        employees: this.toUploadEmployees,
      },
    }).pipe(map(x => x.data.saveEmployees));
  }

  errorToast(message, position, status) {
    this.toastrService.show(
      message || 'Error',
      `Formato incorrecto`,
      {position, status});
  }

  public getEmployeeFormat() {
    const data = [
      {
        'Numero de Empleado': 'Numero de Empleado(Si es un nuevo empleado dejar vacio)',
        Nombre: 'Nombre del Empleado',
        'Apellido Paterno': 'Apellido Paterno',
        'Apellido Materno': 'Apellido Materno',
        Numero: 'Numero de Telefono',
        Area: 'Area',
        Supervisor: 'Supervisor',
        Puesto: 'Puesto',
        Turno: 'Turno',
        Genero: 'Género',
      },
    ];
    const options = {
      filename: 'Formato Empleados',
      showLabels: true,
      useTextFile: false,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(data);
  }

  public exportEmployees(x: any, error: String) {
    this.errorReport.push(
      {
        'ID Empleado': x[0],
        Nombre: x[1],
        'Apellido Paterno': x[2],
        'Apellido Materno': x[3],
        Numero: x[4],
        Error: error,
      });
  }
}
