import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {map} from 'rxjs/operators';
import * as _ from 'lodash';
import {
  Employee,
  Mutation,
  MutationSaveAnswersArgs,
  MutationSaveEmployeesArgs,
  Role,
  User,
} from '../../../generated/graphql';
import {Query} from '../../../generated/graphql';
import {NbToastrService} from '@nebular/theme';
import {newArray} from '@angular/compiler/src/util';
import {NbAuthResult} from '@nebular/auth';
import * as moment from 'moment';

@Component({
  selector: 'ngx-users',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
})
export class QuizComponent implements OnInit {
  public status: string = 'quiz';
  submitted: boolean = false;
  employee: Employee = {};
  messages: string[] = [];
  errors: string[] = [];
  showMessages: any = {};
  public answers: any = [
    [false, false, false, false],
    [false, false, false, false, false],
    [false, false], [false, false],
    [false, false],
  ];

  constructor(private apollo: Apollo,
              private toastrService: NbToastrService,
  ) { }

  ngOnInit(): void {
    const query = this.apollo.query<Query>({
      query: gql`query {
        employee {
          id
          name
          middleName
          lastName
          area
          supervisor
          position
          shift
          answers {
            id
            idEmployee
            answerOne
            answerTwo
            answerThree
            answerFour
            answerFive
            status
          }
        }
      }`,
    }).pipe(map(response => response.data.employee)).subscribe(employee => {
      if (!employee) return;

      this.employee = employee;
      const answer = this.employee.answers;

      this.answers = [
        _(answer.answerOne).split(',').map(v => v === 'true').value(),
        _(answer.answerTwo).split(',').map(v => v === 'true').value(),
        _(answer.answerThree).split(',').map(v => v === 'true').value(),
        _(answer.answerFour).split(',').map(v => v === 'true').value(),
        _(answer.answerFive).split(',').map(v => v === 'true').value(),
      ];

      this.status = answer.status;
    });
  }

  public allowSave() {
    return true;
    // return (this.answers[0].includes(true) &&
    // this.answers[1].includes(true) &&
    // this.answers[2].includes(true) &&
    // this.answers[3].includes(true) &&
    // this.answers[4].includes(true))
  }

  public approved() {
    this.saveAnswers();
  }

  public saveAnswers() {
    this.status = this.answers[0][3] === true &&
    this.answers[1][4] === true &&
    this.answers[2][0] === true &&
    this.answers[3][0] === true &&
    this.answers[4][0] === true ? 'approved' : 'denied';

    this.onSaveAnswers().subscribe(res => {
      if (res) {
        this.status = this.answers[0][3] === true &&
        this.answers[1][4] === true &&
        this.answers[2][0] === true &&
        this.answers[3][0] === true &&
        this.answers[4][0] === true ? 'approved' : 'denied';
      } else {
        this.status = 'denied';
        // mostrar mensaje de error al guardar, intentar nuevamente en un momento
      }
    });
  }

  public onSaveAnswers(): Observable<boolean> {
    return this.apollo.mutate<Mutation, MutationSaveAnswersArgs>({
      mutation: gql`mutation ($answers: answersInput) {
        saveAnswers(answers: $answers)
      }`,
      variables: {
        answers: {
          idEmployee: 1,
          answerOne: this.answers[0].toString(),
          answerTwo: this.answers[1].toString(),
          answerThree: this.answers[2].toString(),
          answerFour: this.answers[3].toString(),
          answerFive: this.answers[4].toString(),
          status: this.status,
          created: moment().toISOString(),
        },
      },
    }).pipe(map(x => x.data.saveAnswers));
  }

  public unselectAll(a, b, $event) {
    if ($event) {
      for (let i = 0; i < 3 + a; i++) {
        this.answers[a][i] = false;
      }
    }
  }

  public clickedOption(a, $event) {
    if ($event)
      this.answers[a][3 + a] = false;
  }

  public login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;
  }
}
