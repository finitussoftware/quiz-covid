import { NgModule } from '@angular/core';
import {
  NbAlertModule,
  NbButtonGroupModule,
  NbButtonModule,
  NbCardModule, NbCheckboxModule,
  NbDatepickerModule, NbFormFieldModule, NbIconModule,
  NbInputModule, NbListModule,
  NbMenuModule, NbRadioModule,
  NbSelectModule, NbTabsetModule,
  NbTimepickerModule, NbToggleModule,
} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import {FormsModule} from '@angular/forms';
import {ChartModule} from 'angular2-chartjs';
import {FileUploadModule} from 'ng2-file-upload';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {QuizComponent} from './quiz/quiz.component';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import { ReportsModule } from './reports/reports.module';

@NgModule({
  imports: [
    SweetAlert2Module,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbDatepickerModule,
    NbTimepickerModule,
    FileUploadModule,
    NbButtonModule,
    NgxDatatableModule,
    ChartModule,
    NbSelectModule,
    NbFormFieldModule,
    NbIconModule,
    NbListModule,
    NbTabsetModule,
    NbButtonGroupModule,
    NbToggleModule,
    NbRadioModule,
    NbCheckboxModule,
    NbAlertModule,
    ReportsModule,
  ],
  declarations: [
    PagesComponent,
    QuizComponent,
  ],
})
export class PagesModule {
}
