 import {Component, OnInit} from '@angular/core';
import {NbTokenService} from '@nebular/auth';
import { MENU_ITEMS } from './pages-menu';
import * as _ from 'lodash';
import {ActivatedRoute, Router} from '@angular/router';
import {Apollo} from 'apollo-angular';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  menu = MENU_ITEMS;
  constructor (
    private apollo: Apollo,
    private router: Router,
    private tokenService: NbTokenService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.tokenService.get().subscribe(token => {
      this.hideMenu(_.map(token.getPayload().auth, 'authority'), token);
      //const rol = token.getPayload().role.toUpperCase();
      //let destiny;
      //rol === 'USER' || rol === 'TERMINAL' ? destiny = 'registration' : rol === 'PDV' ? destiny = 'sales' : null;
      //destiny ?
      //this.router.navigate(['pages/' + destiny]) : null;
      this.router.navigate(['pages/quiz'])
    });

  }

  hideMenu(permissions: string[], token: any): void {
    for (const m of this.menu) {
    m.hidden = _.has(m, 'data.permission') && !permissions.includes(_.get(m, 'data.permission'));
    }
  }
}
