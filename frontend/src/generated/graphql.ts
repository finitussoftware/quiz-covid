export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Bytes: any;
  Date: any;
  Time: any;
}



export interface Answers {
  id?: Maybe<Scalars['Int']>;
  idEmployee?: Maybe<Scalars['Int']>;
  created?: Maybe<Scalars['Date']>;
  answerOne?: Maybe<Scalars['String']>;
  answerTwo?: Maybe<Scalars['String']>;
  answerThree?: Maybe<Scalars['String']>;
  answerFour?: Maybe<Scalars['String']>;
  answerFive?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
}



export interface Employee {
  id?: Maybe<Scalars['Int']>;
  employeeNumber?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  number?: Maybe<Scalars['String']>;
  area?: Maybe<Scalars['String']>;
  supervisor?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  shift?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  answers?: Maybe<Answers>;
}

export interface EmployeeInput {
  id?: Maybe<Scalars['Int']>;
  employeeNumber?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  number?: Maybe<Scalars['String']>;
  area?: Maybe<Scalars['String']>;
  supervisor?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  shift?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
}

export interface Mutation {
  deleteUser?: Maybe<Scalars['Boolean']>;
  saveAnswers?: Maybe<Scalars['Boolean']>;
  saveEmployees?: Maybe<Scalars['Boolean']>;
  saveUser?: Maybe<Scalars['Boolean']>;
}


export interface MutationDeleteUserArgs {
  id?: Maybe<Scalars['Int']>;
}


export interface MutationSaveAnswersArgs {
  answers?: Maybe<AnswersInput>;
}


export interface MutationSaveEmployeesArgs {
  employees?: Maybe<Array<Maybe<EmployeeInput>>>;
}


export interface MutationSaveUserArgs {
  user?: Maybe<UserInput>;
}

export interface Query {
  employee?: Maybe<Employee>;
  employees?: Maybe<Array<Maybe<Employee>>>;
  rolById?: Maybe<Scalars['Int']>;
  roles?: Maybe<Array<Maybe<Role>>>;
  user?: Maybe<User>;
  users?: Maybe<Array<Maybe<User>>>;
}


export interface QueryRolByIdArgs {
  id?: Maybe<Scalars['Int']>;
}


export interface QueryUserArgs {
  id?: Maybe<Scalars['Int']>;
}

export interface Role {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
}


export interface User {
  id?: Maybe<Scalars['Int']>;
  username?: Maybe<Scalars['String']>;
}

export interface UserInput {
  id?: Maybe<Scalars['Int']>;
  username?: Maybe<Scalars['String']>;
  rolId?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
}

export interface AnswersInput {
  id?: Maybe<Scalars['Int']>;
  idEmployee?: Maybe<Scalars['Int']>;
  created?: Maybe<Scalars['Date']>;
  answerOne?: Maybe<Scalars['String']>;
  answerTwo?: Maybe<Scalars['String']>;
  answerThree?: Maybe<Scalars['String']>;
  answerFour?: Maybe<Scalars['String']>;
  answerFive?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
}
