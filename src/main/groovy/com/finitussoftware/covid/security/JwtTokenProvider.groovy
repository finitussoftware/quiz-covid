package com.finitussoftware.covid.security


import com.finitussoftware.covid.services.UsersService
import io.jsonwebtoken.Claims
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpClientErrorException

import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest

@Component
class JwtTokenProvider {
	@Value("\${security.jwt.token.secret-key:secret-key}")
	private String secretKey = "123"

	@Value("\${security.jwt.token.expire-length:86400000}")
	private long accessValidityInMilliseconds = 1000 * 60 * 60 * 24 // 24h

	@Value("\${security.jwt.token.refresh-expire-length:259200000}")
	private long refreshValidityInMilliseconds = 1000 * 60 * 60 * 24 * 3 // 3 days


	private UsersService usersService

	@Autowired
	private MyUserDetails myUserDetails

    JwtTokenProvider(UsersService usersService){
		this.usersService = usersService
	}


	@PostConstruct
	protected void init() {
		secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes())
	}

	public String accessToken(String username, Collection<GrantedAuthority> roles) {
		 return createToken(username, roles, accessValidityInMilliseconds)
	}

	public String createToken(String username, Collection<GrantedAuthority> roles, long validTime) {
		Claims claims = Jwts.claims().setSubject(username)
		claims.put("auth", roles)
		claims.put("userId", 1)
		//claims.put("role",usersService.getRolesByUserId(usersService.getUserIdByUsername(username))[0].name)

		Date now = new Date()
		Date validity = new Date(now.getTime() + validTime)

		return Jwts.builder()
				.setClaims(claims)
				.setIssuedAt(now)
				.setExpiration(validity)
				.signWith(SignatureAlgorithm.HS256, secretKey)
				.compact()

	}

	public String refreshToken(String username, Collection<GrantedAuthority> roles) {
		return createToken(username, roles, refreshValidityInMilliseconds)
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = myUserDetails.loadUserByUsername(getUsername(token))
		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities())
	}

	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject()
	}

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization")
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7)
		}

		bearerToken = req.getParameter("token")
		if(bearerToken != null) {
			return bearerToken
		}

		return null
	}

	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token)
			return true
		} catch (JwtException | IllegalArgumentException ignored) {
			throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Expired or invalid JWT token")
		}
	}
}
