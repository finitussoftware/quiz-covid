package com.finitussoftware.covid.security


import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

import java.util.stream.Collectors


import static com.finitussoftware.covid.jooq.tables.Accounts.ACCOUNTS
import static com.finitussoftware.covid.jooq.tables.Permissions.PERMISSIONS
import static com.finitussoftware.covid.jooq.tables.RolePermissions.ROLE_PERMISSIONS
import static com.finitussoftware.covid.jooq.tables.Roles.ROLES
import static com.finitussoftware.covid.jooq.tables.Users.USERS
import static com.finitussoftware.covid.jooq.tables.UsersRole.USERS_ROLE

@Service
class MyUserDetails implements UserDetailsService{
	private DSLContext dslContext

	@Autowired
	MyUserDetails(DSLContext dslContext){
		this.dslContext = dslContext
	}

	@Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		def query = dslContext.select().from(USERS)
				.innerJoin(ACCOUNTS).on(ACCOUNTS.ID.eq(USERS.ID))
				.where(ACCOUNTS.USERNAME.eq(username))
		def results = query.fetchOne()

		if (results == null) throw new UsernameNotFoundException()

		def roles = (dslContext.select().from(ROLES)
				.innerJoin(USERS_ROLE).on(USERS_ROLE.ROLE_ID.eq(ROLES.ID))
				.leftJoin(ROLE_PERMISSIONS).on(ROLE_PERMISSIONS.ROLE_ID.eq(ROLES.ID))
				.innerJoin(PERMISSIONS).on(PERMISSIONS.ID.eq(ROLE_PERMISSIONS.PERMISSION_ID)) | ROLES.NAME.eq("admin"))
				.where(USERS_ROLE.USER_ID.eq(results.get(USERS.ID)))
				.fetch(PERMISSIONS.NAME).stream()
				.map({role -> new SimpleGrantedAuthority(role)})
				.collect(Collectors.toList())

		return User
				.withUsername(username)
				.password(results.get(ACCOUNTS.PASSWORD))
				.authorities(roles)
				.accountExpired(false)
				.accountLocked(false)
				.credentialsExpired(false)
				.disabled(!results.get(USERS.ACTIVE))
				.build()
	}
}
