package com.finitussoftware.covid.classes

public class MailInlineContent {
	public String name
	public byte[] content

	public static MailInlineContent fromFile(File file) {
		return new MailInlineContent(name: file.name, content: file.bytes)
	}
}
