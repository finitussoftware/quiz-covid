package com.finitussoftware.covid.classes

class MailAddress {
	public String name
	public String mailAddress

	@Override
	public String toString() {
		if (!name) return mailAddress
		return "${name} <${mailAddress}>"
	}
}
