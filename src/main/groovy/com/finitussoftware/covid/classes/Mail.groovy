package com.finitussoftware.covid.classes

class Mail {
	public String subject
	public MailAddress from
	public List<MailAddress> to
	public List<MailAddress> cc
	public String message
	public MailInlineContent inlineContent
}
