package com.finitussoftware.covid.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.xhtmlrenderer.pdf.ITextRenderer

@Service
class PDFService {
    @Autowired
    private TemplateEngine templateEngine

    public void createPdf(OutputStream outputStream) {
        Context context = new Context()
//        context.setVariable("name", "leofres.com")
        String processHtml = templateEngine.process("helloworld", context)

        ITextRenderer renderer = new ITextRenderer()
        renderer.setDocumentFromString(processHtml)
        renderer.layout()
        renderer.createPDF(outputStream)
        renderer.finishPDF()
        outputStream.close()
    }
}
