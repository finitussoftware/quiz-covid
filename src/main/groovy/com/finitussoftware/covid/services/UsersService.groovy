package com.finitussoftware.covid.services

import com.finitussoftware.covid.graphql.input.*
import com.finitussoftware.covid.jooq.tables.pojos.*
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.jooq.impl.SQLDataType
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import static com.finitussoftware.covid.jooq.tables.Accounts.ACCOUNTS
import static com.finitussoftware.covid.jooq.tables.Permissions.PERMISSIONS
import static com.finitussoftware.covid.jooq.tables.RolePermissions.ROLE_PERMISSIONS
import static com.finitussoftware.covid.jooq.tables.Roles.ROLES
import static com.finitussoftware.covid.jooq.tables.Users.USERS
import static com.finitussoftware.covid.jooq.tables.UsersRole.USERS_ROLE

import static org.jooq.impl.DSL.function
import static org.jooq.impl.DSL.inline

@Service
class UsersService {
	private DSLContext dslContext

	UsersService(DSLContext dslContext) {
		this.dslContext = dslContext
	}

	public List<Users> getUsers() {
		def query = (dslContext.select(USERS.asterisk()).from(USERS)
				.innerJoin(ACCOUNTS).on(ACCOUNTS.ID.eq(USERS.ID)) & ACCOUNTS.ACTIVE.eq(true))
				.where(USERS.ACTIVE.eq(true))

		def results = query.fetchInto(Users)

		return results
	}

	public Integer getRol(int id) {
		def query = dslContext.select(USERS_ROLE.ROLE_ID).from(USERS_ROLE)
				.where(USERS_ROLE.USER_ID.eq(id))

		def results = query.fetchInto(UsersRole)

		return results[0].roleId
	}

	public List<Accounts> getAccounts() {
		def query = (dslContext.select(ACCOUNTS.asterisk()).from(USERS)
				.innerJoin(ACCOUNTS).on(ACCOUNTS.ID.eq(USERS.ID)) & ACCOUNTS.ACTIVE.eq(true))
				.where(USERS.ACTIVE.eq(true))

		def results = query.fetchInto(Accounts)

		return results
	}

	public boolean deleteUser(int id) {
		dslContext.update(ACCOUNTS)
				.set(ACCOUNTS.ACTIVE, false)
				.where(ACCOUNTS.ID.eq(id)).execute()
		return true
	}

	public Accounts getAccount(Integer id) {
		def query = dslContext.select().from(ACCOUNTS).where(ACCOUNTS.ID.eq(id)).fetchOneInto(Accounts)
		return query
	}

	public Users getUserById(int id) {
		def query = dslContext.select().from(USERS)
				.innerJoin(ACCOUNTS).on(ACCOUNTS.ID.eq(USERS.ID))
				.where(USERS.ID.eq(id))

		def results = query.fetchOneInto(Users)

		return results
	}

	public List<Permissions> getPermissionsByUsername(String username) {
		def query = dslContext.select(PERMISSIONS.fields()).from(USERS)
				.innerJoin(ACCOUNTS).on(ACCOUNTS.ID.eq(USERS.ID))
				.innerJoin(USERS_ROLE).on(USERS_ROLE.USER_ID.eq(USERS.ID))
				.innerJoin(ROLE_PERMISSIONS).on(ROLE_PERMISSIONS.ROLE_ID.eq(USERS_ROLE.ROLE_ID))
				.innerJoin(PERMISSIONS).on(PERMISSIONS.ID.eq(ROLE_PERMISSIONS.PERMISSION_ID))
				.where(ACCOUNTS.USERNAME.eq(username))

		def results = query.fetchInto(Permissions)

		return results
	}

	public List<Roles> getRolesByUserId(Integer userId) {
		def query = dslContext
				.select(ROLES.asterisk()).from(USERS)
				.innerJoin(USERS_ROLE).on(USERS_ROLE.USER_ID.eq(USERS.ID))
				.innerJoin(ROLES).on(ROLES.ID.eq(USERS_ROLE.ROLE_ID))
				.where(USERS.ID.eq(userId))

		def results = query.fetchInto(Roles)

		return results
	}

	public Integer getUserIdByUsername(String username) {
		def query = dslContext.select(USERS.ID).from(USERS)
				.innerJoin(ACCOUNTS).on(ACCOUNTS.ID.eq(USERS.ID))
				.where(ACCOUNTS.USERNAME.eq(username)) & USERS.ACTIVE.eq(true)

		def results = query.fetchOne(USERS.ID)
		return results
	}

	public List<Roles> getRoles() {
		def query = dslContext.select().from(ROLES)
		def results = query.fetchInto(Roles)
		return results
	}

	@Transactional
	boolean saveUser(UserInput inputUser) {
		if (inputUser.id != null) {
			def result = dslContext.update(USERS_ROLE)
					.set(USERS_ROLE.ROLE_ID, inputUser.rolId)
					.where(USERS_ROLE.USER_ID.eq(inputUser.id))

			result.execute()
			if(!inputUser.password){
				dslContext.update(ACCOUNTS).set(ACCOUNTS.USERNAME, inputUser.username)
						.where(ACCOUNTS.ID.eq(inputUser.id)).execute()
			}
			else{
				dslContext.update(ACCOUNTS)
						.set(ACCOUNTS.PASSWORD, function("crypt", String, inline(inputUser.password), function("gen_salt", String, inline("bf"), inline(12))))
						.set(ACCOUNTS.USERNAME, inputUser.username)
						.where(ACCOUNTS.ID.eq(inputUser.id))
						.execute()
			}
			return true
		} else {
			def userId = dslContext.insertInto(USERS)
					.columns(USERS.CREATED, USERS.ACTIVE)
					.values(DSL.now().cast(SQLDataType.TIMESTAMPWITHTIMEZONE), DSL.val(true))
					.returning().fetchOne().get(USERS.ID)

			dslContext.insertInto(ACCOUNTS)
					.set(ACCOUNTS.ID, userId)
					.set(ACCOUNTS.USERNAME, inputUser.username)
					.set(ACCOUNTS.PASSWORD, function("crypt", String, inline(inputUser.password), function("gen_salt", String, inline("bf"), inline(12))))
					.execute()

			dslContext.insertInto(USERS_ROLE)
					.columns(USERS_ROLE.USER_ID, USERS_ROLE.ROLE_ID, USERS_ROLE.CREATED_BY)
					.values(userId, inputUser.rolId, 1)
					.execute()

		}
		return true
	}
}