package com.finitussoftware.covid.services

import com.finitussoftware.covid.classes.Mail
import com.finitussoftware.covid.classes.MailAddress
import com.finitussoftware.covid.graphql.input.AnswersInput
import org.jooq.DSLContext
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service

import java.security.Principal

import static com.finitussoftware.covid.jooq.tables.Answers.ANSWERS

@Service
class AnswersService {

    private DSLContext dslContext
    private MailSenderService mailSenderService
    private EmployeesService employeesService

    AnswersService(DSLContext dslContext, MailSenderService mailSenderService, EmployeesService employeesService) {
        this.employeesService = employeesService
        this.mailSenderService = mailSenderService
        this.dslContext = dslContext
    }

    public boolean  saveAnswers(AnswersInput answers){
        def principal = SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal() as UserDetails
        def employee = employeesService.getEmployeeById(principal.username)

        answers.idEmployee = principal.username
        dslContext.newRecord(ANSWERS, answers).insert()

        def mail = new Mail()
        mail.subject = "Notificación Covid"
        mail.from = new MailAddress(name: "No Reply", mailAddress: "no_reply@mail.finitussoftware.com")
        mail.to = [new MailAddress(name: "Eric Leon Ortiz", mailAddress: "eric.leonortiz@finitussoftware.com"), new MailAddress(name: "Rubi Arroyo", mailAddress: "rubi.arroyo@finitussoftware.com")]
        def data = [
                "Empleado": "${employee.name} ${employee.middleName} ${employee.lastName}".toString(),
                "NumEmpleado": employee.employeeNumber
        ]

        mailSenderService.sendMail(mail, "notificacion-covid", data)

        return true
    }
}
