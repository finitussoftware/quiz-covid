package com.finitussoftware.covid.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.finitussoftware.covid.classes.Mail
import kong.unirest.HttpRequestWithBody
import kong.unirest.HttpResponse
import kong.unirest.JsonNode
import kong.unirest.UnirestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MailSenderService {
    private UnirestInstance unirest
    private ObjectMapper objectMapper
    private final static APP_DOMAIN = "mail.finitussoftware.com"
    private final static API_KEY = "2ab3acd3989ae0f2ddb6026c40bf314d-29561299-376809f0"

    @Autowired
    MailSenderService(UnirestInstance unirest, ObjectMapper objectMapper){
        this.unirest = unirest
        this.objectMapper = objectMapper
    }

    private HttpRequestWithBody getHttpRequestBuilder() {
        return unirest.post("https://api.mailgun.net/v3/" + APP_DOMAIN + "/messages")
                .basicAuth("api", API_KEY)
    }

    private HttpRequestWithBody getHttpRequestBuilder(Mail mail) {
        return getHttpRequestBuilder()
                .queryString("from", mail.from.mailAddress)
                .queryString("to", mail.to.join(","))
                .queryString("subject", mail.subject)
    }

    private HttpRequestWithBody getHttpRequestBuilder(Mail mail, String template) {
        return getHttpRequestBuilder(mail)
                .queryString("template", template)
    }

    public JsonNode sendMail(Mail mail) {
        HttpResponse<JsonNode> request = getHttpRequestBuilder(mail)
                .queryString("text", mail.message)
                .asJson()

        return request.getBody()
    }

    public JsonNode sendMail(Mail mail, String template) {
        HttpResponse<JsonNode> request = getHttpRequestBuilder(mail, template)
                .asJson()

        return request.getBody()
    }

    public JsonNode sendMail(Mail mail, String template, Object data) {
        HttpResponse<JsonNode> request = getHttpRequestBuilder(mail, template)
                .field("h:X-Mailgun-Variables", objectMapper.writeValueAsString(data))
                .asJson()

        return request.getBody()
    }
}
