package com.finitussoftware.covid.services

import com.finitussoftware.covid.security.JwtTokenProvider
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpServerErrorException
import sun.reflect.generics.reflectiveObjects.NotImplementedException

import javax.servlet.http.HttpServletRequest
import java.util.stream.Collectors

import static com.finitussoftware.covid.jooq.tables.Accounts.ACCOUNTS
import static org.jooq.impl.DSL.function
import static org.jooq.impl.DSL.inline

@Service
class AuthService {
	private DSLContext dslContext

	public AuthService(DSLContext dslContext) {
		this.dslContext = dslContext
	}

	private PasswordEncoder passwordEncoder

	private JwtTokenProvider jwtTokenProvider

	private AuthenticationManager authenticationManager

	private UsersService usersService

	@Autowired
    AuthService(PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager, UsersService usersService){
		this.passwordEncoder = passwordEncoder
		this.jwtTokenProvider = jwtTokenProvider
		this.authenticationManager = authenticationManager
		this.usersService = usersService
	}

	public Map<String, String> signin(String username, String password) {
		try {
			def results = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password))
			return generateTokens(username, results)
		} catch (AuthenticationException e) {
			throw new HttpServerErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid username/password supplied")
		}
	}

	private LinkedHashMap<String, String> generateTokens(String username, Authentication results) {
		def accessToken = jwtTokenProvider.accessToken(username, results.authorities)
		def refreshToken = jwtTokenProvider.refreshToken(username, results.authorities)

		return [accessToken: accessToken, refreshToken: refreshToken]
	}

	public String signup(User user) {
		throw new NotImplementedException()
//		if (!userRepository.existsByUsername(user.getUsername())) {
//			user.setPassword(passwordEncoder.encode(user.getPassword()))
//			userRepository.save(user)
//			return jwtTokenProvider.createToken(user.getUsername(), user.getRoles())
//		} else {
//			throw new HttpServerErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Username is already in use")
//		}
	}

	public void delete(String username) {
//		userRepository.deleteByUsername(username)
		throw new NotImplementedException()
	}

	public User search(String username) {
//		User user = userRepository.findByUsername(username)
		User user = null
		if (user == null) {
			throw new HttpServerErrorException(HttpStatus.NOT_FOUND, "The user doesn't exist")
		}
		return user
	}

	public User whoami(HttpServletRequest req) {
//		return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)))
		throw new NotImplementedException()
	}

	public Map<String, String> refresh(String token) {
		def something = jwtTokenProvider.validateToken(token)
		def foo = jwtTokenProvider.getAuthentication(token)
		def permissions = usersService.getPermissionsByUsername(foo.name).stream()
				.map({role -> new SimpleGrantedAuthority(role.name)})
				.collect(Collectors.toList())

		return generateTokens(foo.name, foo)
	}

}
