package com.finitussoftware.covid.services

import com.finitussoftware.covid.graphql.input.EmployeeInput
import com.finitussoftware.covid.graphql.input.UserInput
import com.finitussoftware.covid.jooq.tables.pojos.Answers
import com.finitussoftware.covid.jooq.tables.pojos.Employees
import org.jooq.DSLContext
import org.springframework.stereotype.Service

import java.sql.Timestamp
import java.time.Instant
import java.time.temporal.ChronoUnit

import static com.finitussoftware.covid.jooq.tables.Answers.ANSWERS
import static com.finitussoftware.covid.jooq.tables.Employees.EMPLOYEES

@Service
class EmployeesService {
    private UsersService usersService
    private DSLContext dslContext

    EmployeesService(DSLContext dslContext, UsersService usersService) {
        this.dslContext = dslContext
        this.usersService = usersService
    }

    public List<Employees> employees(){
        def row = dslContext.select().from(EMPLOYEES).fetchInto(Employees)
        return row
    }

    public Employees getEmployeeById(String employeeId) {
        def exist = dslContext.select().from(EMPLOYEES).where(EMPLOYEES.EMPLOYEE_NUMBER.eq(employeeId)).fetchOneInto(Employees)
        return exist
    }

    public boolean  saveEmployees(List<EmployeeInput> employees ){
        employees.forEach{x ->
            def row = dslContext.newRecord(EMPLOYEES, x)
            def exist = dslContext.select().from(EMPLOYEES).where(EMPLOYEES.EMPLOYEE_NUMBER.eq(x.employeeNumber)).fetchOneInto(Employees)
            if(exist == null){
                row.insert()
                UserInput user = []
                user.username = x.employeeNumber
                user.rolId = 3
                user.password = x.employeeNumber
                usersService.saveUser(user)
            } else {
                row.id = exist.id
                row.update()
            }
        }
        return true
    }

    public Answers getAnswersByEmployeeId(String employeeId) {
        def today = Timestamp.from(Instant.now().truncatedTo(ChronoUnit.DAYS))
        def query = dslContext
                .select().from(ANSWERS)
                .where(ANSWERS.ID_EMPLOYEE.eq(Integer.parseInt(employeeId)))
                .and(ANSWERS.CREATED.greaterOrEqual(today))

        def results = query.fetchOneInto(Answers)
        return results
    }
}
