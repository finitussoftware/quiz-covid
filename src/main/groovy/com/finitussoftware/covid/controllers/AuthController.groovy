package com.finitussoftware.covid.controllers

import com.finitussoftware.covid.services.AuthService
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/auth")
class AuthController {
	private AuthService authService
	private DSLContext dslContext

	public AuthController(DSLContext dslContext) {
		this.dslContext = dslContext
	}
	@Autowired
    AuthController(AuthService authService){
		this.authService = authService
	}

	@PostMapping("/signin")
	public ResponseEntity login(@RequestBody Map<String, Object> user) {
		def token = authService.signin(user["email"] as String, user["password"] as String)
		return ResponseEntity.ok([data: [key: token["accessToken"]]])
	}

	@DeleteMapping("/logout")
	public ResponseEntity logout(){
		return ResponseEntity.ok().build()
	}

	@PostMapping("/refresh")
	public String refresh(@RequestBody Map<String, Object> user) {
		def token = authService.refresh(user["refresh_token"] as String)
		return ResponseEntity.ok([token: token])
	}
}
