package com.finitussoftware.covid.controllers


import com.finitussoftware.covid.services.PDFService
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.font.PDType1Font
import org.jooq.DSLContext
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.servlet.http.HttpServletResponse
import java.sql.Timestamp
import java.time.Instant
import java.time.temporal.ChronoUnit

import static com.finitussoftware.covid.jooq.tables.Answers.ANSWERS
import static com.finitussoftware.covid.jooq.tables.Employees.EMPLOYEES

@RequestMapping("api/covid")
@RestController
class CovidController {
    private DSLContext dslContext

    private PDFService pdfService

    public CovidController(DSLContext dslContext, PDFService pdfService) {
        this.dslContext = dslContext
        this.pdfService = pdfService
    }

    @GetMapping("answers")
    public List<Map<String, Object>> getEmployeesAnswers() {
        def today = Timestamp.from(Instant.now().truncatedTo(ChronoUnit.DAYS))
        def employees = dslContext.select([*EMPLOYEES.fields(), ANSWERS.STATUS]).from(ANSWERS)
                .innerJoin(EMPLOYEES).on(EMPLOYEES.EMPLOYEE_NUMBER
                .eq(ANSWERS.ID_EMPLOYEE.cast(String)))
                .where(ANSWERS.CREATED.greaterOrEqual(today))
                .fetchMaps()

        return employees
    }

    @PostMapping("answers")
    public HttpEntity<Boolean> saveAnswer(@RequestParam Integer employeeId, @RequestParam String status) {
        def today = Timestamp.from(Instant.now().truncatedTo(ChronoUnit.DAYS))

        dslContext.update(ANSWERS)
                .set(ANSWERS.STATUS, status)
                .where(ANSWERS.ID_EMPLOYEE.eq(employeeId))
                .and(ANSWERS.CREATED.greaterOrEqual(today))
                .execute()

        return ResponseEntity.ok(true)
    }

    @GetMapping("document")
    public void getDocument(HttpServletResponse response, @RequestParam Integer employeeId) {
        response.addHeader("content-type", "application/pdf")

        pdfService.createPdf(response.getOutputStream())
    }
}
