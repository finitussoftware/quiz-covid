package com.finitussoftware.covid.configurators

import kong.unirest.Unirest
import kong.unirest.UnirestInstance
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MailConfiguration {

	@Bean()
	public UnirestInstance unirest() {
		return Unirest.primaryInstance()
	}
}
