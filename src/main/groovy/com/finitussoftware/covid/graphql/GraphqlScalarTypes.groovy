package com.finitussoftware.covid.graphql

import graphql.schema.Coercing
import graphql.schema.CoercingParseLiteralException
import graphql.schema.CoercingParseValueException
import graphql.schema.CoercingSerializeException
import graphql.schema.GraphQLScalarType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import java.sql.Time
import java.text.SimpleDateFormat
import java.time.OffsetDateTime

@Configuration
class GraphqlScalarTypes {
	@Bean
	GraphQLScalarType bytesScalar() {
		return new GraphQLScalarType.Builder()
				.name("Bytes")
				.description("Binary data base64 encoded")
				.coercing(new Coercing<byte[], String>() {
					@Override
					String serialize(Object dataFetcherResult) throws CoercingSerializeException {
						return Base64.getEncoder().encode(dataFetcherResult as byte[])
					}

					@Override
					byte[] parseValue(Object input) throws CoercingParseValueException {
						return Base64.getDecoder().decode(input as String)
					}

					@Override
					byte[] parseLiteral(Object input) throws CoercingParseLiteralException {
						return Base64.getDecoder().decode(input as String)
					}
				}).build()
	}

	@Bean()
	GraphQLScalarType dateScalar(){
		return new GraphQLScalarType.Builder()
				.name("Date")
				.description("Una bonita fecha")
				.coercing(new Coercing<OffsetDateTime, String>() {
					@Override
					String serialize(Object dataFetcherResult) throws CoercingSerializeException {
						return dataFetcherResult.toString()
					}

					@Override
					OffsetDateTime parseValue(Object input) throws CoercingParseValueException {
						if(!input instanceof String) return null

						return OffsetDateTime.parse(input as CharSequence)
					}

					@Override
					OffsetDateTime parseLiteral(Object input) throws CoercingParseLiteralException {
						if(!input instanceof String) return null

						return OffsetDateTime.parse(input as CharSequence)
					}
				}).build()
	}

	@Bean()
	GraphQLScalarType timeScalar(){
		return new GraphQLScalarType.Builder()
				.name("Time")
				.description("Una bonita hora")
				.coercing(new Coercing<Time, String>() {
					@Override
					String serialize(Object dataFetcherResult) throws CoercingSerializeException {
						new SimpleDateFormat("HH:mm").format(dataFetcherResult)
					}

					@Override
					Time parseValue(Object input) throws CoercingParseValueException {
						if(!input instanceof String) return null

						return new Time(new SimpleDateFormat("HH:mm").parse(input as String).getTime())
					}

					@Override
					Time parseLiteral(Object input) throws CoercingParseLiteralException {
						if(!input instanceof String) return null

						return new Time(new SimpleDateFormat("HH:mm").parse(input as String).getTime())
					}
				}).build()
	}
}
