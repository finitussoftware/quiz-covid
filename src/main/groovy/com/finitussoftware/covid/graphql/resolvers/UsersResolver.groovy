package com.finitussoftware.covid.graphql.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver
import com.finitussoftware.covid.graphql.input.UserInput
import com.finitussoftware.covid.jooq.tables.pojos.Accounts
import com.finitussoftware.covid.jooq.tables.pojos.Roles
import com.finitussoftware.covid.services.UsersService
import org.springframework.stereotype.Component

@Component
class UsersResolver implements GraphQLResolver<Accounts> {
}

@Component
class UsersQueryResolver implements GraphQLQueryResolver, GraphQLMutationResolver{
    private UsersService usersService
    UsersQueryResolver(UsersService usersService){
        this.usersService = usersService
    }

    public List<Accounts> users() {
        return usersService.getAccounts()
    }

    public Accounts user(Integer id){
        return usersService.getAccount(id)
    }

    public Integer rolById(int id) {
        return usersService.getRol(id)
    }

    public Boolean saveUser(UserInput user){
        return usersService.saveUser(user)
    }

    public List<Roles> roles() {
        return usersService.getRoles()
    }

    public Boolean deleteUser(Integer id){
        return usersService.deleteUser(id)
    }
}
