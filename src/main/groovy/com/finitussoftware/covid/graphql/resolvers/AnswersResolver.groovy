package com.finitussoftware.covid.graphql.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver
import com.finitussoftware.covid.graphql.input.AnswersInput
import com.finitussoftware.covid.graphql.input.EmployeeInput
import com.finitussoftware.covid.jooq.tables.pojos.Answers
import com.finitussoftware.covid.services.AnswersService
import com.finitussoftware.covid.services.EmployeesService
import org.springframework.stereotype.Component

class AnswersResolver  implements GraphQLResolver<Answers> {
}

@Component
class AnswersQueryResolver implements  GraphQLQueryResolver, GraphQLMutationResolver{
    private AnswersService answersService
    AnswersQueryResolver(AnswersService answersService){
        this.answersService = answersService
    }

    public boolean saveAnswers(AnswersInput answers){
        return answersService.saveAnswers(answers)
    }

}