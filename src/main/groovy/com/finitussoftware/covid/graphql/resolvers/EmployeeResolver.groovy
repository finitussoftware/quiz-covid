package com.finitussoftware.covid.graphql.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver
import com.finitussoftware.covid.graphql.input.EmployeeInput
import com.finitussoftware.covid.jooq.tables.pojos.Answers
import com.finitussoftware.covid.jooq.tables.pojos.Employees
import com.finitussoftware.covid.services.EmployeesService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component

@Component
class EmployeeResolver implements GraphQLResolver<Employees> {
    private EmployeesService employeesService

    EmployeeResolver(EmployeesService employeesService){
        this.employeesService = employeesService
    }

    public Answers answers(Employees employee) {
        def result = employeesService.getAnswersByEmployeeId(employee.employeeNumber)

        return result
    }
}

@Component
class EmployeeQueryResolver implements  GraphQLQueryResolver, GraphQLMutationResolver {

    private EmployeesService employeesService
    EmployeeQueryResolver(EmployeesService employeesService){
        this.employeesService = employeesService
    }

    public List<Employees> employees(){
        return employeesService.employees()
    }

    public Employees employee() {
        def principal = SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal() as UserDetails
        return employeesService.getEmployeeById(principal.username)
    }

    public boolean saveEmployees(List<EmployeeInput> employees){
        return employeesService.saveEmployees(employees)
    }
}