package com.finitussoftware.covid.graphql.input

class AnswersInput {
    public Integer id
    public String idEmployee
    public String answerOne
    public String answerTwo
    public String answerThree
    public String answerFour
    public String answerFive
    public Date created
    public String status
}
