package com.finitussoftware.covid.graphql.input

class   EmployeeInput {
    public Integer id
    public String employeeNumber
    public String name
    public String middleName
    public String lastName
    public String number
    public String area
    public String supervisor
    public String position
    public String shift
    public String gender
}
