package com.finitussoftware.covid.graphql.dataloaders

import graphql.kickstart.execution.context.GraphQLContext
import graphql.schema.DataFetchingEnvironment
import org.dataloader.DataLoader

abstract class BaseResolver {
	@SuppressWarnings("GrMethodMayBeStatic")
	public <K, V> DataLoader<K, V> getDataLoader(DataFetchingEnvironment dataFetchingEnvironment, Closure<Map<K, V>> dataLoaderMethod) {
		def dfe = dataFetchingEnvironment.context as GraphQLContext
		def dataLoader = dfe.dataLoaderRegistry.get().getDataLoader("${dataLoaderMethod.owner.class.name}.${dataLoaderMethod.method}")
		return dataLoader as DataLoader<K, V>
	}
}
