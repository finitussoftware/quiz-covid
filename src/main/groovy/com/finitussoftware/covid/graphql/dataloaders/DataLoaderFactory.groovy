package com.finitussoftware.covid.graphql.dataloaders

import org.dataloader.BatchLoader
import org.dataloader.DataLoader
import org.dataloader.MappedBatchLoader
import org.springframework.stereotype.Component

import java.util.concurrent.CompletableFuture

@Component
class DataLoaderFactory {
	public <K, V> DataLoader<K, V> newDataLoader(Closure<List<V>> function) {
		return DataLoader.newDataLoader({List<K> keys ->
			return CompletableFuture.supplyAsync({->
				return function(keys)
			})
		} as BatchLoader<K, V>)
	}
	public <K, V> DataLoader<K, V> newMapDataLoader(Closure<Map<K, V>> function) {
		return DataLoader.newMappedDataLoader({Set<K> keys ->
			return CompletableFuture.supplyAsync({->
				return function(keys)
			})
		} as MappedBatchLoader<K, V>)
	}
}
