package com.finitussoftware.covid.graphql

import com.coxautodev.graphql.tools.ObjectMapperConfigurer
import com.coxautodev.graphql.tools.ObjectMapperConfigurerContext
import com.coxautodev.graphql.tools.SchemaParserOptions
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.finitussoftware.covid.graphql.annotations.DataLoaderMap
import graphql.kickstart.execution.context.DefaultGraphQLContext
import graphql.kickstart.execution.context.GraphQLContext
import graphql.servlet.context.DefaultGraphQLServletContext
import graphql.servlet.context.DefaultGraphQLWebSocketContext
import graphql.servlet.context.GraphQLServletContextBuilder
import org.dataloader.BatchLoader
import org.dataloader.DataLoader
import org.dataloader.DataLoaderRegistry
import org.dataloader.MappedBatchLoader
import org.reflections.Reflections
import org.reflections.scanners.MethodAnnotationsScanner
import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.BeanFactoryPostProcessor
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory
import org.springframework.beans.factory.support.DefaultListableBeanFactory
import org.springframework.beans.factory.support.GenericBeanDefinition
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.context.WebApplicationContext
import org.springframework.web.context.support.WebApplicationContextUtils

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.websocket.Session
import javax.websocket.server.HandshakeRequest
import java.lang.reflect.Method
import java.util.concurrent.CompletableFuture

@Component
class GraphqlConfiguration implements GraphQLServletContextBuilder, BeanFactoryPostProcessor {
	private Map<String, DataLoader> loaderList
	private WebApplicationContext ctx

	@Override
	public GraphQLContext build(HttpServletRequest req, HttpServletResponse response) {
		ctx = WebApplicationContextUtils.getWebApplicationContext(req.servletContext)
		loaderList = ctx.getBeansOfType(DataLoader)
		return DefaultGraphQLServletContext.createServletContext(buildDataLoaderRegistry(), null).with(req).with(response)
				.build()
	}

	@Override
	public GraphQLContext build() {
		return new DefaultGraphQLContext(buildDataLoaderRegistry(), null)
	}

	@Override
	public GraphQLContext build(Session session, HandshakeRequest request) {
		return DefaultGraphQLWebSocketContext.createWebSocketContext(buildDataLoaderRegistry(), null).with(session)
				.with(request).build()
	}

	private DataLoaderRegistry buildDataLoaderRegistry() {
		DataLoaderRegistry dataLoaderRegistry = new DataLoaderRegistry()
		dataLoaderRegistryBuild(loaderList, dataLoaderRegistry)
		return dataLoaderRegistry
	}

	void dataLoaderRegistryBuild(Map<String, DataLoader<?, ?>> loaderList, DataLoaderRegistry dataLoaderRegistry) {
		for (Map.Entry<String, DataLoader<?, ?>> loader : loaderList) {
			dataLoaderRegistry.register(loader.key, loader.value)
		}
	}

	@Bean
	SchemaParserOptions schemaParserOptions() {
		return SchemaParserOptions.newOptions().objectMapperConfigurer(new ObjectMapperConfigurer() {
			@Override
			void configure(ObjectMapper mapper, ObjectMapperConfigurerContext context) {
				mapper.registerModule(new JavaTimeModule())
			}
		}).build()
	}

	@Override
	void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		def methods = new Reflections("com.leofres.restaurant.services", new MethodAnnotationsScanner())
				.getMethodsAnnotatedWith(DataLoaderMap)

		for (Method method in methods) {
			def beanDefinition = new GenericBeanDefinition()
			beanDefinition.setScope(WebApplicationContext.SCOPE_REQUEST)
			beanDefinition.setBeanClass(DataLoader)
			beanDefinition.setInstanceSupplier({ Method innerMethod ->
				{ ->
					def serviceBean = beanFactory.getBean(innerMethod.declaringClass)
					Closure<Map> dataLoaderSupplier = serviceBean.&"${innerMethod.name}"
					def dataLoader = newMapDataLoader(dataLoaderSupplier)
					return dataLoader
				}
			}.call(method))

			((DefaultListableBeanFactory) beanFactory)
					.registerBeanDefinition("${method.declaringClass.name}.${method.name}", beanDefinition)
		}
	}

	public <K, V> DataLoader<K, V> newMapDataLoader(Closure<Map<K, V>> function) {
		return DataLoader.newMappedDataLoader({ Set<K> keys ->
			return CompletableFuture.supplyAsync({->
				return function(keys)
			})
		} as MappedBatchLoader<K, V>)
	}

	public <K, V> DataLoader<K, V> newDataLoader(Closure<List<V>> function) {
		return DataLoader.newDataLoader({ List<K> keys ->
			return CompletableFuture.supplyAsync({->
				return function(keys)
			})
		} as BatchLoader<K, V>)
	}
}
